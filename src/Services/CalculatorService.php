<?php
namespace Jakmall\Recruitment\Calculator\Services;

class CalculatorService
{

    public function calculate($result,$number,$operation){
        switch ($operation) {
            case '+': $result += $number; break;
            case '-': $result  = $result - $number; break;
            case '*': $result  = $result * $number; break;
            case '/': $result  = $result / $number; break;
            case '^': $result  = pow($result, $number); break;
            default:
                break;
        };
        return $result;
    }



}
