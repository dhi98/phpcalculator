<?php
namespace Jakmall\Recruitment\Calculator\Commands;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Jakmall\Recruitment\Calculator\Commands\Command;
use Jakmall\Recruitment\Calculator\Services\CalculatorService;

class SubtractCommand extends Command
{

    public function __construct()
    {
        parent::__construct();
        $this-> setName('subtract');
        $this->_operation = "-";
    }

}
