<?php
namespace Jakmall\Recruitment\Calculator\Commands;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Jakmall\Recruitment\Calculator\Commands\Command;
use Jakmall\Recruitment\Calculator\Services\CalculatorService;

class PowerCommand extends Command
{

    public function __construct()
    {
        parent::__construct();
        $this-> setName('power');
        $this->_operation = "^";
    }

    protected function _validate($arguments){
        if(count($arguments)>2){
            return "Too many arguments";
        }
    }

}
