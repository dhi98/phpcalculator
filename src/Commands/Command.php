<?php
namespace Jakmall\Recruitment\Calculator\Commands;

use Jakmall\Recruitment\Calculator\Services\CalculatorService;
use Symfony\Component\Console\Command\Command as SymfonyCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class Command extends SymfonyCommand
{
    protected $operation = "";

    public function __construct()
    {
        parent::__construct();
        $this -> setDescription('add commands')
        -> setHelp('This command allows you to calculate some data')
        -> addArgument('numbers',  InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'The username of the user.');
    }

    protected function _validate($arguments){

    }
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $numbers = $input->getArgument('numbers');
        $text = 'Should print result: '.implode(' '.$this->_operation.' ', $numbers);
        $result = 0;
        $result = $numbers[0];
        $calculateService = new CalculatorService;
        $validateMassage = $this->_validate($numbers);
        if($validateMassage != ""){
          return $output->writeln($validateMassage);
        }
        unset($numbers[0]);
        if (count($numbers) > 0) {
            foreach ($numbers as $numb) {
                $result = $calculateService->calculate($result,$numb,$this->_operation);
            }
        }
        return $output->writeln($text.' = '.$result);
    }


}
